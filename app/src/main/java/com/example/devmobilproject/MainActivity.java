package com.example.devmobilproject;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.widget.*;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private Button btn_register;
    private Button btn_connect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.welcome);
        mediaPlayer.start();

        btn_register = findViewById(R.id.btn_register);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register_page = new Intent(MainActivity.this, RegisterActivity.class);
                mediaPlayer.stop();
                startActivity(register_page);
            }
        });

        btn_connect=findViewById(R.id.btn_connect);
        btn_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent accueiljeu_page = new Intent(MainActivity.this, AcceuilJeuActivity.class);
                mediaPlayer.stop();
                startActivity(accueiljeu_page);
            }
        });
    }
}

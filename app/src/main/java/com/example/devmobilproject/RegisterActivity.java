package com.example.devmobilproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

public class RegisterActivity extends AppCompatActivity {
    private Button btn_inscription;
    @Override
    protected void onCreate(Bundle SavedInstancedState)
    {
        super.onCreate(SavedInstancedState);
        setContentView(R.layout.activity_register);

        btn_inscription=findViewById(R.id.btn_inscription);
        btn_inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent accueil_page = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(accueil_page);
            }
        });
    }
}
